# Pacejka94
import numpy as np
from lmfit import minimize, Parameters, Minimizer, report_fit


class P94Lateral:
    def __init__(self):
        self._a0 = 0
        self._a1 = 0
        self._a2 = 0
        self._a3 = 0
        self._a4 = 0
        self._a5 = 0
        self._a6 = 0
        self._a7 = 0
        self._a8 = 0
        self._a9 = 0
        self._a10 = 0
        self._a11 = 0
        self._a12 = 0
        self._a13 = 0
        self._a14 = 0
        self._a15 = 0
        self._a16 = 0
        self._a17 = 0
        
    def _initialize_lateral_values_(self):
        self._a0 = 1.4
        self._a1 = 0
        self._a2 = 1100
        self._a3 = 1100
        self._a4 = 10
        self._a5 = 0
        self._a6 = 0
        self._a7 = -2
        self._a8 = 0
        self._a9 = 0
        self._a10 = 1
        self._a11 = 0
        self._a12 = 0
        self._a13 = 0
        self._a14 = 0
        self._a15 = 0
        self._a16 = 0
        self._a17 = 0

    def _calculate_lateral_force_(self, Fz, SA, SR, IA, P):
        C = self.a0
        D = Fz * (self.a1 * Fz + self.a2) * (1 - self.a15 * IA * IA)
        BCD = self.a3 * np.sin(np.arctan(Fz / self.a4) * 2) * (1 - self.a5 * np.abs(IA))
        B = BCD / (C * D)
        H = self.a8 * Fz + self.a9 + self.a10 * IA
        E = (self.a6 * Fz + self.a7) * (1 - (self.a16 * IA + self.a17) * np.sign(SA + H))
        V = self.a11 * Fz + self.a12 + (self.a13 * Fz + self.a14) * IA * Fz
        Bx1 = B * (SA + H)

        F = D * np.sin(C * np.arctan(Bx1 - E * (Bx1 - np.arctan(Bx1)))) + V

        return F

    @property
    def a0(self):
        return self._a0

    @a0.setter
    def a0(self, value):
        self._a0 = value

    @property
    def a1(self):
        return self._a1

    @a1.setter
    def a1(self, value):
        self._a1 = value

    @property
    def a2(self):
        return self._a2

    @a2.setter
    def a2(self, value):
        self._a2 = value

    @property
    def a3(self):
        return self._a3

    @a3.setter
    def a3(self, value):
        self._a3 = value

    @property
    def a4(self):
        return self._a4

    @a4.setter
    def a4(self, value):
        self._a4 = value

    @property
    def a5(self):
        return self._a5

    @a5.setter
    def a5(self, value):
        self._a5 = value

    @property
    def a6(self):
        return self._a6

    @a6.setter
    def a6(self, value):
        self._a6 = value

    @property
    def a7(self):
        return self._a7

    @a7.setter
    def a7(self, value):
        self._a7 = value

    @property
    def a8(self):
        return self._a8

    @a8.setter
    def a8(self, value):
        self._a8 = value

    @property
    def a9(self):
        return self._a9

    @a9.setter
    def a9(self, value):
        self._a9 = value

    @property
    def a10(self):
        return self._a10

    @a10.setter
    def a10(self, value):
        self._a10 = value

    @property
    def a11(self):
        return self._a11

    @a11.setter
    def a11(self, value):
        self._a11 = value

    @property
    def a12(self):
        return self._a12

    @a12.setter
    def a12(self, value):
        self._a12 = value

    @property
    def a13(self):
        return self._a13

    @a13.setter
    def a13(self, value):
        self._a13 = value

    @property
    def a14(self):
        return self._a14

    @a14.setter
    def a14(self, value):
        self._a13 = value

    @property
    def a15(self):
        return self._a15

    @a15.setter
    def a15(self, value):
        self._a15 = value

    @property
    def a16(self):
        return self._a16

    @a16.setter
    def a16(self, value):
        self._a16 = value

    @property
    def a17(self):
        return self._a13

    @a17.setter
    def a17(self, value):
        self._a17 = value